<?php
/**
 * @package n3t META
 * @author Pavel Poles - n3t.cz
 * @copyright (c) 2017 - 2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\CMS\Factory;
use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Version;
use Joomla\CMS\Filesystem\File;

final class plgSystemN3tMeta extends CMSPlugin
{
  /**
   * Inherited. Load language files automatically.
   *
   * @var boolean
   *
   * @since 1.0.0
   */
  protected $autoloadLanguage = true;

  /**
   * Set in constructor to true if this is frontend and is HTML
   *
   * @var bool
   *
   * @since 4.0.0
   */
  private $canProcess = false;

  /**
   * Application object
   *
   * @var \Joomla\CMS\Application\CMSApplicationInterface|null
   *
   * @since 1.0.0
   */
  private $app = null;

  /**
   * Document object
   *
   * @var \Joomla\CMS\Document\Document|null
   *
   * @since 1.0.0
   */
  private $doc = null;

  /**
   * Input filter for cleaning values
   *
   * @var InputFilter
   *
   * @since 1.0.0
   */
  private $filter = null;

  /**
   * Hold set metadata to check legacy plugins code
   *
   * @var array
   *
   * @since 1.0.0
   */
  private $metaData = [];

  /**
   * Hold array type metadata
   *
   * @var array
   *
   * @since 1.0.0
   */
  private $metaDataArray = [];

  /**
   * Hold all used OpenGraphs prefixes
   *
   * @var array
   *
   * @since 4.0.0
   */
  private $prefixes = [];

  /**
   * Constructor
   *
   * @param $subject
   * @param $config
   * @throws Exception
   *
   * @since 1.0.0
   */
  function __construct(&$subject, $config)
  {
    parent::__construct($subject, $config);

    $this->app = Factory::getApplication();
    $this->doc = Factory::getDocument();
    $this->canProcess = $this->app->isClient('site')
      && ($this->doc->getType() == 'html');

    if (!$this->canProcess)
      return;

    $this->filter = new InputFilter();

    if ($this->params->get('og', 1)) {
      $this->setMetaData('og:type', $this->params->get('og_type', 'website'));
      $this->setMetaData('og:title', $this->params->get('og_title', $this->app->getCfg('sitename')));
      $this->setOgImage($this->params->get('og_image', ''), $this->params->get('twitter_title', $this->app->getCfg('sitename')));
      $this->setMetaData('og:description', $this->params->get('og_description', $this->app->getCfg('MetaDesc')));
      $this->setMetaData('og:url', JUri::current());
      $this->setMetaData('og:site_name', $this->app->getCfg('sitename'));
      $this->setMetaData('og:locale', str_replace('-', '_', Factory::getLanguage()->getTag()));
      $this->setMetaData('fb:app_id', $this->params->get('fb_app_id', ''));
    }

    if ($this->params->get('twitter', 1)) {
      $this->setMetaData('twitter:card', $this->params->get('twitter_type', 'summary_large_image'), false);
      $this->setMetaData('twitter:title', $this->params->get('twitter_title', $this->app->getCfg('sitename')), false);
      $this->setTwitterImage($this->params->get('twitter_image', ''), $this->params->get('twitter_title', $this->app->getCfg('sitename')));
      $this->setMetaData('twitter:description', $this->params->get('twitter_description', $this->app->getCfg('MetaDesc')), false);
      $this->setMetaData('twitter:site', $this->params->get('twitter_site', ''), false);
      $this->setMetaData('twitter:creator', $this->params->get('twitter_creator', ''), false);
    }

    $this->prefixes['og'] = 'https://ogp.me/ns#';
    $this->prefixes['article'] = 'https://ogp.me/ns/article#';
    $this->prefixes['fb'] = 'http://ogp.me/ns/fb#';
  }

  /**
   * Process Joomla article
   *
   * @param $article
   *
   * @since 1.0.0
   */
  protected function contentArticle($article)
  {
    $tags = array();
    foreach ($article->tags->itemTags as $tag)
      $tags[] = $tag->title;

    if ($this->params->get('metadesc_auto', 1) && empty($article->metadesc))
      $article->metadesc = HTMLHelper::_('string.truncate', $article->text, $this->params->get('metadesc_length', 160), true, false);

    if ($this->params->get('metakey_tags', 1)) {
      if ($article->metakey)
        $article->metakey .= ', ';
      $article->metakey .= implode(', ', $tags);
    }

    if ($this->params->get('metakey_extra', '')) {
      if ($article->metakey)
        $article->metakey .= ', ';
      $article->metakey .= $this->params->get('metakey_extra', '');
    }

    if ($this->doc->getType() != 'html') return;

    $images = json_decode($article->images);
    $articleImage = null;
    if (isset($images->image_fulltext) && !empty($images->image_fulltext))
      $articleImage = $images->image_fulltext;
    elseif (isset($images->image_intro) && !empty($images->image_intro))
      $articleImage = $images->image_intro;
    if ($articleImage)
      $articleImage = ltrim($articleImage, '/');

    if ($this->params->get('og', 1)) {
      $this->setMetaData('og:type', 'article');

      $this->setMetaData('og:title', $this->cleanStr($article->title));

      if ($article->metadesc)
        $this->setMetaData('og:description', $this->cleanStr($article->metadesc, $this->params->get('metadesc_length', 160)));
      else
        $this->setMetaData('og:description', $this->cleanStr($article->text, $this->params->get('metadesc_length', 160)));

      $this->setOgImage($articleImage, $article->title);

      $dateCreated = null;
      if ($article->publish_up && !empty($article->publish_up))
        $dateCreated = $article->publish_up;
      elseif ($article->created && !empty($article->created))
        $dateCreated = $article->created;

      $timezone = new DateTimeZone($this->app->getCfg('offset'));
      if ($dateCreated) {
        $dateCreated = new JDate($dateCreated);
        $dateCreated->setTimezone($timezone);
        $this->setMetaData('article:published_time', $dateCreated->toISO8601(true));
      }

      if ($article->modified) {
        $dateModified = new JDate($article->modified);
        $dateModified->setTimezone($timezone);
        $this->setMetaData('article:modified_time', $dateModified->toISO8601(true));
      }

      if ($article->publish_down) {
        $datePublishDown = new JDate($article->publish_down);
        $datePublishDown->setTimezone($timezone);
        $this->setMetaData('article:expiration_time', $datePublishDown->toISO8601(true));
      }

      $this->setMetaData('article:author', $this->params->get('og_author', ''));
      $this->setMetaData('article:publisher', $this->params->get('og_publisher', ''));
      $this->setMetaData('article:section', $article->category_title);
      $this->setMetaData('article:tag', $tags);
    }

    if ($this->params->get('twitter', 1)) {
      $this->setMetaData('twitter:title', $article->title, false);
      $this->setTwitterImage($articleImage, $article->title);
      if ($article->metadesc)
        $this->setMetaData('twitter:description', $this->cleanStr($article->metadesc, $this->params->get('metadesc_length', 160)), false);
      else
        $this->setMetaData('twitter:description', $this->cleanStr($article->text, $this->params->get('metadesc_length', 160)), false);
    }
  }

  /**
   * Process Joomla com_category
   *
   * @param $category
   *
   * @since 1.0.0
   */
  private function contentCategory($category)
  {
    $tags = array();
    foreach ($category->tags->itemTags as $tag)
      $tags[] = $tag->title;

    if ($this->params->get('metadesc_auto', 1) && empty($category->metadesc))
      $category->metadesc = $this->cleanStr($category->text, $this->params->get('metadesc_length', 160));

    if ($this->params->get('metakey_tags', 1)) {
      if ($category->metakey)
        $category->metakey .= ', ';
      $category->metakey .= implode(', ', $tags);
    }

    if ($this->params->get('metakey_extra', '')) {
      if ($category->metakey)
        $category->metakey .= ', ';
      $category->metakey .= $this->params->get('metakey_extra', '');
    }

    if ($this->doc->getType() != 'html') return;

    $images = json_decode($category->images);
    $categoryImage = null;
    if (isset($images->image) && !empty($images->image))
      $categoryImage = $images->image;
    if ($categoryImage)
      $categoryImage = ltrim($categoryImage, '/');

    if ($this->params->get('og', 1)) {
      $this->setMetaData('og:type', 'website');

      $this->setMetaData('og:title', $this->cleanStr($category->title));

      if ($category->metadesc)
        $this->setMetaData('og:description', $this->cleanStr($category->metadesc, $this->params->get('metadesc_length', 160)));
      else
        $this->setMetaData('og:description', $this->cleanStr($category->text, $this->params->get('metadesc_length', 160)));

      $this->setOgImage($categoryImage, $category->title);
    }

    if ($this->params->get('twitter', 1)) {
      $this->setMetaData('twitter:title', $this->cleanStr($category->title), false);
      $this->setTwitterImage($categoryImage, $category->title);
      if ($category->metadesc)
        $this->setMetaData('twitter:description', $this->cleanStr($category->metadesc, $this->params->get('metadesc_length', 160)), false);
      else
        $this->setMetaData('twitter:description', $this->cleanStr($category->text, $this->params->get('metadesc_length', 160)), false);
    }
  }

  /**
   * Joomla event handler
   *
   * @param string $context
   * @param $item
   * @param $params
   * @param int $page
   * @throws Exception
   *
   * @since 1.0.0
   */
  public function onContentAfterDisplay(string $context, &$item, &$params, $page = 0)
  {
    $input = Factory::getApplication()->input;
    switch ($context) {
      case 'com_content.archive':
        break;
      case 'com_content.article':
        if ($this->app->input->getCmd('option') == 'com_content' && $this->app->input->getCmd('view') == 'article' && $this->app->input->getInt('id') == $item->id)
          $this->contentArticle($item);
        break;
      case 'com_content.category':
        // need to call this elsewhere, category do not trigger AfterDisplay
        /*if ($this->app->input->getCmd('option') == 'com_content' && $this->app->input->getCmd('view') == 'category' && $this->app->input->getInt('id') == $item->id)
          $this->contentCategory($item);
        break;*/
      case 'com_content.featured':
        break;
    }
  }

  /**
   * Joomla Event Handler
   * @since 1.0.0
   */
  public function onAfterDispatch()
  {
    if (!$this->canProcess)
      return;

    /*
      detect legacy tags added by addCustomTag
    */
    for ($index = count($this->doc->_custom); $index > 0; $index--) {
      $custom = $this->doc->_custom[$index - 1];
      if (preg_match('~<meta\s*(property|name)="(.*)"\s*content="(.*)"\s*/>$~i', $custom, $matches)) {
        if (isset($this->metaData[$matches[2]])) {
          $this->doc->setMetadata($matches[2], $matches[3], $matches[1]);
          $this->metaData[$matches[2]] = $matches[3];
          unset($this->doc->_custom[$index - 1]);
        } elseif (isset($this->metaDataArray[$matches[2]])) {
          unset($this->metaDataArray[$matches[2]]);
        }
      }
    }

    /*
       add array META data
    */
    foreach ($this->metaDataArray as $name => $meta)
      foreach ($meta['values'] as $value)
        $this->doc->addCustomTag('<meta ' . $meta['type'] . '="' . $name . '" content="' . htmlspecialchars($value, ENT_COMPAT, 'UTF-8') . '" />');

    /*
       add prefixes head
    */
    $prefixes = [];
    foreach ($this->prefixes as $namespace => $url)
      $prefixes[] = $namespace . ': ' . $url;
    $this->doc->addCustomTag('<head prefix="' . implode(' ', $prefixes) . '" />');
  }

  /**
   * Cleans and shorten string for META usage
   *
   * @param $value
   * @param int $length
   * @return string
   *
   * @since 1.0.0
   */
  private function cleanStr(?string $value, int $length = 0): string
  {
    if ($length)
      $value = HTMLHelper::_('string.truncate', $value, $length, true, false);

    $value = $this->filter->clean($value);
    return html_entity_decode($value, ENT_COMPAT | ENT_HTML401, 'UTF-8');
  }

  /**
   * Stores string or Array META data value
   *
   * @param string $name
   * @param $value
   * @param bool $property
   *
   * @since 1.0.0
   */
  private function setMetaData(string $name, $value, bool $property = true): void
  {
    if (!$this->canProcess)
      return;

    if (is_string($value))
      $value = trim($value);

    if (is_array($value)) {
      $this->metaDataArray[$name] = [
        'type' => $property ? 'property' : 'name',
        'values' => $value,
      ];
    } else if ($value) {
      $this->doc->setMetadata($name, $value, $property ? 'property' : 'name');
      $this->metaData[$name] = $value;
    }
  }


  /**
   * Sets OpenGraph Image related tags
   *
   * @param $url
   *
   * @since 4.0.0
   */
  private function setOgImage(?string $url, ?string $alt): void
  {
    if (Version::MAJOR_VERSION > 3)
      $url = HTMLHelper::cleanImageURL($url)->url;

    $file = JPATH_BASE . '/' . $url;
    if ($url && File::exists($file)) {
      try {
        $image = JImage::getImageFileProperties($file);
        $this->setMetaData('og:image', JUri::base(false) . $url);
        $this->setMetaData('og:image:type', $image->mime);
        $this->setMetaData('og:image:width', $image->width);
        $this->setMetaData('og:image:height', $image->height);
      } catch (\Exception $e) {
      }
    } else
      $this->setMetaData('og:image', $url);
    $this->setMetaData('og:image:alt', $this->cleanStr($alt));
  }

  /**
   * Sets Twitter Image related tags
   *
   * @param string $url
   * @param string $alt
   *
   * @since 4.0.0
   */
  private function setTwitterImage(?string $url, ?string $alt): void
  {
    if (Version::MAJOR_VERSION > 3)
      $url = HTMLHelper::cleanImageURL($url)->url;

    $url = $this->cleanStr($url);
    $file = JPATH_BASE . '/' . $url;
    if ($url && File::exists($file))
      $this->setMetaData('twitter:image', JUri::base(false) . $url, false);
    else
      $this->setMetaData('twitter:image', $url, false);
    $this->setMetaData('twitter:image:alt', $this->cleanStr($alt), false);
  }
}
