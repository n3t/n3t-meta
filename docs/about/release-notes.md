Release notes
=============

4.0.x
-----

#### 4.0.0

- Initial public release
- Joomla 4 compatibility
- minimal PHP version 7.2
- new article:tag, article:modified_time and article:expiration_time META tags support
- new og:image:alt META tag support
- add head tag with prefix namespaces for specification compatibility