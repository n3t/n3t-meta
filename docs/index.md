n3t META
========

n3t META is Joomla! plugin, which adds additional META tags (like Open Graph or Twitter cards) to Joomla! output.

Install instructions
----------------------------
 * install the plugin using Joomla! installer
 * enable plugin and set basic options
 * clear Joomla! Cache, if enabled
 * check HTML output of your site

__More documentation coming soon...__